Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2010, SURFnet bv / 2010
License: BSD-2-clause

Files: aclocal.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: compile
 depcomp
 missing
 test-driver
Copyright: 1996-2020, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2010,2015 Ondřej Surý <ondrej@debian.org>
License: Expat

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: m4/ax_cxx_compile_stdcxx_11.m4
Copyright: 2014, Alexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: m4/ltversion.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/pkg.m4
Copyright: 2012-2015, Dan Nicholson <dbn.lists@gmail.com>
 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: modules/*
Copyright: no-info-found
License: public-domain

Files: modules/FindSQLite3.cmake
Copyright: file distributed with LuaDist.
 2007-2009, LuaDist.
License: Expat

Files: src/bin/*
Copyright: no-info-found
License: BSD-2-clause

Files: src/bin/common/findslot.cpp
 src/bin/common/findslot.h
Copyright: 2010, 2012, 2013, 2016-2018, SURFnet bv
License: BSD-2-clause

Files: src/bin/dump/*
Copyright: no-info-found
License: BSD-2-clause

Files: src/bin/keyconv/base64.c
Copyright: 1996-1999, Internet Software Consortium.
License: ISC

Files: src/lib/*
Copyright: 2010, 2012, 2013, 2016-2018, SURFnet bv
License: BSD-2-clause

Files: src/lib/P11Attributes.cpp
 src/lib/P11Attributes.h
 src/lib/P11Objects.cpp
 src/lib/P11Objects.h
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/SoftHSM.cpp
Copyright: 2010, SURFnet bv / 2010
License: BSD-2-clause

Files: src/lib/access.cpp
 src/lib/access.h
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/common/SimpleConfigLoader.cpp
 src/lib/common/SimpleConfigLoader.h
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/common/osmutex.cpp
 src/lib/common/osmutex.h
Copyright: 2010, SURFnet bv / 2008-2010
License: BSD-2-clause

Files: src/lib/crypto/BotanAES.cpp
 src/lib/crypto/BotanAES.h
 src/lib/crypto/BotanDES.cpp
 src/lib/crypto/BotanDES.h
 src/lib/crypto/BotanDH.cpp
 src/lib/crypto/BotanDH.h
 src/lib/crypto/BotanDHKeyPair.cpp
 src/lib/crypto/BotanDHKeyPair.h
 src/lib/crypto/BotanDHPrivateKey.cpp
 src/lib/crypto/BotanDHPrivateKey.h
 src/lib/crypto/BotanDHPublicKey.cpp
 src/lib/crypto/BotanDHPublicKey.h
 src/lib/crypto/BotanDSA.cpp
 src/lib/crypto/BotanDSA.h
 src/lib/crypto/BotanDSAKeyPair.cpp
 src/lib/crypto/BotanDSAKeyPair.h
 src/lib/crypto/BotanDSAPrivateKey.cpp
 src/lib/crypto/BotanDSAPrivateKey.h
 src/lib/crypto/BotanDSAPublicKey.cpp
 src/lib/crypto/BotanDSAPublicKey.h
 src/lib/crypto/BotanECDH.cpp
 src/lib/crypto/BotanECDH.h
 src/lib/crypto/BotanECDHKeyPair.cpp
 src/lib/crypto/BotanECDHKeyPair.h
 src/lib/crypto/BotanECDHPrivateKey.cpp
 src/lib/crypto/BotanECDHPrivateKey.h
 src/lib/crypto/BotanECDHPublicKey.cpp
 src/lib/crypto/BotanECDHPublicKey.h
 src/lib/crypto/BotanECDSA.cpp
 src/lib/crypto/BotanECDSA.h
 src/lib/crypto/BotanECDSAKeyPair.cpp
 src/lib/crypto/BotanECDSAKeyPair.h
 src/lib/crypto/BotanECDSAPrivateKey.cpp
 src/lib/crypto/BotanECDSAPrivateKey.h
 src/lib/crypto/BotanECDSAPublicKey.cpp
 src/lib/crypto/BotanECDSAPublicKey.h
 src/lib/crypto/BotanEDDSA.cpp
 src/lib/crypto/BotanEDDSA.h
 src/lib/crypto/BotanEDKeyPair.cpp
 src/lib/crypto/BotanEDKeyPair.h
 src/lib/crypto/BotanEDPrivateKey.cpp
 src/lib/crypto/BotanEDPrivateKey.h
 src/lib/crypto/BotanEDPublicKey.cpp
 src/lib/crypto/BotanEDPublicKey.h
 src/lib/crypto/BotanGOST.cpp
 src/lib/crypto/BotanGOST.h
 src/lib/crypto/BotanGOSTKeyPair.cpp
 src/lib/crypto/BotanGOSTKeyPair.h
 src/lib/crypto/BotanGOSTPrivateKey.cpp
 src/lib/crypto/BotanGOSTPrivateKey.h
 src/lib/crypto/BotanGOSTPublicKey.cpp
 src/lib/crypto/BotanGOSTPublicKey.h
 src/lib/crypto/BotanGOSTR3411.cpp
 src/lib/crypto/BotanGOSTR3411.h
 src/lib/crypto/BotanHashAlgorithm.cpp
 src/lib/crypto/BotanMAC.cpp
 src/lib/crypto/BotanMAC.h
 src/lib/crypto/BotanMD5.cpp
 src/lib/crypto/BotanMD5.h
 src/lib/crypto/BotanMacAlgorithm.cpp
 src/lib/crypto/BotanMacAlgorithm.h
 src/lib/crypto/BotanRNG.cpp
 src/lib/crypto/BotanRNG.h
 src/lib/crypto/BotanRSA.cpp
 src/lib/crypto/BotanRSA.h
 src/lib/crypto/BotanRSAKeyPair.cpp
 src/lib/crypto/BotanRSAKeyPair.h
 src/lib/crypto/BotanRSAPrivateKey.cpp
 src/lib/crypto/BotanRSAPrivateKey.h
 src/lib/crypto/BotanRSAPublicKey.cpp
 src/lib/crypto/BotanRSAPublicKey.h
 src/lib/crypto/BotanSHA1.cpp
 src/lib/crypto/BotanSHA1.h
 src/lib/crypto/BotanSHA224.cpp
 src/lib/crypto/BotanSHA224.h
 src/lib/crypto/BotanSHA256.cpp
 src/lib/crypto/BotanSHA256.h
 src/lib/crypto/BotanSHA384.cpp
 src/lib/crypto/BotanSHA384.h
 src/lib/crypto/BotanSHA512.cpp
 src/lib/crypto/BotanSHA512.h
 src/lib/crypto/BotanSymmetricAlgorithm.cpp
 src/lib/crypto/BotanSymmetricAlgorithm.h
 src/lib/crypto/BotanUtil.h
 src/lib/crypto/OSSLEVPMacAlgorithm.cpp
 src/lib/crypto/OSSLEVPMacAlgorithm.h
 src/lib/crypto/OSSLHMAC.cpp
 src/lib/crypto/OSSLHMAC.h
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/crypto/BotanCryptoFactory.cpp
 src/lib/crypto/BotanCryptoFactory.h
Copyright: 2010, SURFnet bv / 2010
License: BSD-2-clause

Files: src/lib/crypto/BotanHashAlgorithm.h
 src/lib/crypto/BotanUtil.cpp
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/crypto/Botan_ecb.cpp
 src/lib/crypto/Botan_ecb.h
Copyright: 2016, Daniel Neus, Rohde & Schwarz Cybersecurity
 1999-2009, 2013, Jack Lloyd
License: BSD-2-clause

Files: src/lib/crypto/OSSLComp.cpp
Copyright: 2016, SURFnet bv
License: BSD-2-clause and/or OpenSSL

Files: src/lib/crypto/test/chisq.c
Copyright: no-info-found
License: NTP

Files: src/lib/crypto/test/ent.c
 src/lib/crypto/test/iso8859.c
 src/lib/crypto/test/iso8859.h
 src/lib/crypto/test/randtest.c
 src/lib/crypto/test/randtest.h
Copyright: no-info-found
License: NTP

Files: src/lib/pkcs11/cryptoki.h
Copyright: 2010, 2012, 2013, 2016-2018, SURFnet bv
License: BSD-2-clause

Files: src/lib/session_mgr/*
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/session_mgr/test/sessionmgrtest.cpp
Copyright: 2010, 2012, 2013, 2016-2018, SURFnet bv
License: BSD-2-clause

Files: src/lib/test/*
Copyright: no-info-found
License: BSD-2-clause

Files: src/lib/test/AsymEncryptDecryptTests.cpp
 src/lib/test/AsymEncryptDecryptTests.h
 src/lib/test/DeriveTests.cpp
 src/lib/test/DeriveTests.h
 src/lib/test/ObjectTests.cpp
 src/lib/test/ObjectTests.h
 src/lib/test/SignVerifyTests.cpp
 src/lib/test/SignVerifyTests.h
 src/lib/test/SymmetricAlgorithmTests.cpp
 src/lib/test/SymmetricAlgorithmTests.h
Copyright: 2012, 2014, SURFnet
License: BSD-2-clause

Files: src/lib/test/AsymWrapUnwrapTests.cpp
 src/lib/test/AsymWrapUnwrapTests.h
Copyright: 2014, Red Hat
License: BSD-2-clause

Files: src/lib/test/ForkTests.cpp
 src/lib/test/ForkTests.h
Copyright: 2020, Red Hat, Inc.
License: BSD-2-clause

Files: src/lib/test/p11test.cpp
Copyright: 2010, 2012, 2013, 2016-2018, SURFnet bv
License: BSD-2-clause

Files: Makefile.in src/* src/bin/Makefile.in src/bin/common/Makefile.in src/bin/dump/Makefile.in src/bin/keyconv/Makefile.in src/bin/migrate/Makefile.in src/bin/util/Makefile.in src/lib/Makefile.in src/lib/common/Makefile.in src/lib/crypto/Makefile.in src/lib/data_mgr/Makefile.in src/lib/data_mgr/test/Makefile.in src/lib/handle_mgr/Makefile.in src/lib/handle_mgr/test/Makefile.in src/lib/object_store/File.cpp src/lib/object_store/Makefile.in src/lib/object_store/test/Makefile.in src/lib/pkcs11/* src/lib/session_mgr/Makefile.in src/lib/session_mgr/test/Makefile.in src/lib/slot_mgr/Makefile.in src/lib/slot_mgr/test/Makefile.in src/lib/test/Makefile.in
Copyright: 2010 .SE, The Internet Infrastructure Foundation
 2010 SURFnet bv
License: BSD-2-Clause

Files: src/lib/crypto/test/Makefile.in
Copyright: 1985, 2008 John "Random" Walker
 Gary Perlman, Wang Institute, Tyngsboro
License: public-domain
